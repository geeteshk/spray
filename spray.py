from time import sleep
from pyautogui import typewrite, press, hotkey, locateCenterOnScreen, center, click

import sys

try:
    keyword = sys.argv[1]
except:
    print('Usage: python ' + sys.argv[0] + ' <phrase>')
    print('And make sure a person\'s discord conversation is open.')
    sys.exit()

colors = ['diff', 'dts', 'fix', 'css', 'yaml', 'ini', 'bf', 'tex']
prefixes = ['-', '#', '*', 'o', '\\', '[', '&', '$']
counter = 0
charspace = '  '

try:
    barX, barY = locateCenterOnScreen('bar.png')
    click(barX, barY)
except TypeError:
    print('Could not find bar just focus manually and wait...')
    sleep(8)

for i in range(20):
    spaces = ' ' * i
    output = ''
    
    for c in keyword:
        output += c + spaces
    
    output1 = '```' + colors[counter]
    output2 = prefixes[counter] + charspace + output.rstrip()
    output3 = '```'
    
    typewrite(output1)
    hotkey('shift', 'enter')
    
    typewrite(output2)
    hotkey('shift', 'enter')
    
    typewrite(output3)
    press('enter')
    
    counter += 1
    if counter == len(colors):
        counter = 0
    
    sleep((i ** 2) * 0.01)
    